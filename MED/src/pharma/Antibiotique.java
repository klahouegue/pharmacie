package pharma;

public class Antibiotique extends Medicament{
	
	private Bacterie bacterie;
	
	public Antibiotique(int reference, String libelle, double prix, String dateFabrication, Bacterie bacterie) {
		super(reference, libelle, prix, dateFabrication);
		this.bacterie = bacterie;
	}

	public Bacterie getBacterie() {
		return bacterie;
	}

	public void setBacterie(Bacterie bacterie) {
		this.bacterie = bacterie;
	}
	
	public double calculTaxeAppliquee() {
		double taxe = super.calculTaxeAppliquee();
		switch (bacterie) {
		
		case THERMOPHYLE:
			taxe = prix*10/100;
			break;
			
		case MESOPHYLE:
			taxe = prix*12/100;
			break;

		default:
			taxe = prix*15/100;
			break;
		}
		return taxe;
	}
	
	
	public String toString() {
		String antibiotique = "";
		antibiotique += "----------ANTIBIOTIQUE----------" + "\n";
		antibiotique += super.toString() + "\n";
		antibiotique += "bacterie: " + bacterie + "\n";
		antibiotique += "--------------------------------";
		return antibiotique;
	}


}
