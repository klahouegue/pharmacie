package pharma;

public class Test {
	
	
	public static void main(String[] args) {
		
		System.out.println();
		System.out.println("///////////affichages/////////////");
		System.out.println();
		
		Medicament m1 = new Medicament(2000, "Aspegic", 1, "18/06/2013");
		System.out.println(m1);
		System.out.println("la taxe appliqu�e pour Aspegic: " + m1.calculTaxeAppliquee());
		
		System.out.println();
		System.out.println();
		
		Antibiotique a1 = new Antibiotique(4432, "Clamoxyl", 1, "18/02/2010", Bacterie.THERMOPHYLE);
		System.out.println(a1);
		System.out.println("la taxe appliqu�e pour Clamoxyl: " + a1.calculTaxeAppliquee());
		
		System.out.println();
		System.out.println();
		
		AntiInflammatoire ai1 = new AntiInflammatoire(4434 , "Maxilase", 1, "28/04/2010", "steroidien", 8);
		System.out.println(ai1);
		
		
		System.out.println();
		System.out.println();
		
		Homeopatique h1 = new Homeopatique(4435, "oscillococcinum", 1, "23/12/2010", "saccharose");
		System.out.println(h1);
		System.out.println();
		
		
		Medicament[] med = new Medicament[4];
		med[0] = m1;
		med[1] = a1;
		med[2] = ai1;
		med[3] = h1;
		
		System.out.println();
		System.out.println("///////////Parcours du tableau/////////////");
		System.out.println();
		
		for (int i = 0; i < med.length; i++) {
			System.out.println(med[i]);
			System.out.println("Taxe pour "+med[i].libelle+": "+med[i].calculTaxeAppliquee());
			System.out.println();
		}
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		Pharmacie ph = new Pharmacie("TuniPhar");
		for (int i = 0; i < med.length; i++) {
			ph.ajouterMedicament(med[i]);
		}
		
		System.out.println();
		System.out.println("///////////Affichage pharmacie/////////////");
		System.out.println(ph);
		
		ph.supprimerMedicament(4434);
		
		System.out.println();
		System.out.println("///////////Affichage pharmacie apr�s suppression/////////////");
		System.out.println();
		
		System.out.println(ph);
		
	}

}
