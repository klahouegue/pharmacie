package pharma;

public class Homeopatique extends Medicament{
	
	private String plante;

	public Homeopatique(int reference, String libelle, double prix, String dateFabrication, String plante) {
		super(reference, libelle, prix, dateFabrication);
		this.plante = plante;
	}
	
	public double calculTaxeAppliquee() {
		return prix*20/100;
	}

	public String getPlante() {
		return plante;
	}

	public void setPlante(String plante) {
		this.plante = plante;
	}
	
	public String toString() {
		String antibiotique = "";
		antibiotique += "----------HOMEOPATIQUE----------" + "\n";
		antibiotique += super.toString() + "\n";
		antibiotique += "plante: " + plante + "\n";
		antibiotique += "--------------------------------";
		return antibiotique;
	}
	
	
	
	
	
	

}
