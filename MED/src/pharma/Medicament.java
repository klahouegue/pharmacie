package pharma;

public class Medicament {
	
	protected int reference;
	protected String libelle;
	protected double prix;
	protected String dateFabrication;
	
	
	public Medicament(int reference, String libelle, double prix, String dateFabrication) {
		this.reference = reference;
		this.libelle = libelle;
		this.prix = prix;
		this.dateFabrication = dateFabrication;
	}
	
	public double calculTaxeAppliquee(){
		return prix * 18 /100;
	}
	
	public boolean equals(Object obj){
		if(this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Medicament))
			return false;
		Medicament other = (Medicament)obj;
		if ((this.reference == other.reference)&&
				(this.libelle.equals(other.libelle))) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public String toString(){
		String medicament = "";
		medicament += "-----------MEDICAMANT-----------" + "\n";
		medicament += "reference: " + reference + "\n";
		medicament += "libelle: " + libelle + "\n";
		medicament += "prix: " + prix + "\n";
		medicament += "date de fabrication: " + dateFabrication + "\n";
		medicament += "--------------------------------";
		return medicament;
	}

	

	

	

	
	
	
	
	
	
	

}
