package pharma;

public class Pharmacie {
	
	public static final int NB_MAX_MEDICAMENT = 1000;
	
	private String nom;
	private Medicament[] medicaments;
	
	private int nbMedicaments;
	
	
	public Pharmacie(String nom) {
		this.nom = nom;
		this.medicaments = new Medicament[NB_MAX_MEDICAMENT];
		this.nbMedicaments = 0;
	}
	
	
	
	public String toString() {
		String pharmacie = "";
		pharmacie += "-------------PHARMACIE-------------" + "\n";
		pharmacie += "nom: " + nom + "\n";
		pharmacie += "Liste des medicaments:" + "\n";
		for(int i=0; i<nbMedicaments; i++){
			pharmacie += medicaments[i].toString() + "\n\n";
		}
		pharmacie += "-----------------------------------";
		return pharmacie;
	}
	
	public int rechercherIndiceMedicament(int reference){
		int indice = -1;
		boolean trouve = false;
		int i = 0;
		while( (i<nbMedicaments) && (!trouve)){
			if (medicaments[i].reference == reference) {
				indice = i;
				trouve = true;
			}
			i++;
		}
		return indice;
	}
	
	public void ajouterMedicament(Medicament medicament){
		int indice = rechercherIndiceMedicament(medicament.reference);
		if ((nbMedicaments < NB_MAX_MEDICAMENT) &&
				indice == -1) {
			medicaments[nbMedicaments] = medicament;
			nbMedicaments++;
		}else {
			System.out.println("Le medicament existe d�j�");
		}
	}
	
	public void supprimerMedicament(int reference){
		int indice = this.rechercherIndiceMedicament(reference);
		if(indice != -1){
			for(int i = indice+1; i< nbMedicaments; i++){
				medicaments[i-1] = medicaments[i];
			}
			medicaments[nbMedicaments-1] = null;
			nbMedicaments--;
		}else{
			System.out.println("Le medicament n'existe pas");
		}
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public Medicament[] getMedicaments() {
		return medicaments;
	}


	public void setMedicaments(Medicament[] medicaments) {
		this.medicaments = medicaments;
	}
	
	
	
	
	

}
