package pharma;

public class AntiInflammatoire extends Medicament{
	
	private String molecule;
	private int acidite;
	
	
	public AntiInflammatoire(int reference, String libelle, double prix, String dateFabrication, String molecule, int acidite) {
		super(reference, libelle, prix, dateFabrication);
		this.molecule = molecule;
		this.acidite = acidite;
	}
	
	public double calculTaxeAppliquee() {
		
		if (molecule.equals("steroidien")) {
			return prix*10/100;
		} else {
			return prix*15/100;
		}
		
	}


	public String getMolecule() {
		return molecule;
	}


	public void setMolecule(String molecule) {
		this.molecule = molecule;
	}


	public int getAcidite() {
		return acidite;
	}


	public void setAcidite(int acidite) {
		this.acidite = acidite;
	}
	
	
	public String toString() {
		String antibiotique = "";
		antibiotique += "-------ANTI-INFLAMMATOIRE-------" + "\n";
		antibiotique += super.toString() + "\n";
		antibiotique += "molecule: " + molecule + "\n";
		antibiotique += "acidite: " + acidite + "\n";
		antibiotique += "--------------------------------";
		return antibiotique;
	}
	
	
	
	
	

}
